//+-------------------------------------------------------------------------
//
// Carlos Noceda - (c) 2020
//
// File:    text_scroll.h
//
// Description:
//
//  Texto desplazandose en la matriz.
//
//
// History:   20201125  cnoceda   creado
//
//--------------------------------------------------------------------------

#include <time.h>  
#include <Arduino.h>
#define FASTED_INTERNAL
#include <FastLED.h>
#include <FontRobotron.h>

extern long textColor;

static void DrawText(char* txt1){

    // Serial.println("Entramos en DrawText");
    // Serial.print("Txt: ");
    // Serial.println(txt1);
    // Serial.print("Buffer_l: ");
    // Serial.println(strlen(txt1));
    int x = 4;
    static int y = 15;

    // static boolean only_1 = false;
    
    uint8_t m_FontWidth, m_FontBase, m_FWBytes = 0, m_FCBytes, m_FontHeight;
    const uint8_t *m_FontData;
    int leds_w;    
    
    
    leds_w = strlen(txt1) * 7 + strlen(txt1);
    boolean ledChar[BUFFER_LED] = {false}; 

    // Serial.print(sizeof(ledChar)); Serial.print(" "); Serial.println(leds_w);
     
    //init font
    // const uint8_t RobotronFontData[] = {
	// 				7,		// Font Width
	// 				7,		// Font Height
	// 				32,		// Font First Character
	// 				96,		// Font Last Character
    const uint8_t *FontData = RobotronFontData;
    m_FontWidth = FontData[0];
    m_FontHeight = FontData[1];
    m_FontBase = FontData[2];
    // m_FontUpper = FontData[3];
    m_FontData = &FontData[4];
    m_FWBytes = (m_FontWidth + 7) / 8;
    m_FCBytes += (m_FWBytes * m_FontHeight);
    // // Serial.println(m_FontWidth);
    // // Serial.println(m_FontHeight);
    // // Serial.println(m_FontBase);
    // // // Serial.println(m_FontUpper);
    // // Serial.println(m_FWBytes);
    // // Serial.println(m_FCBytes);

    for (int l = 0; l < strlen(txt1); l++)
    {
        // Find char in font in the Font
        // char c = textMsg.charAt(l);
        char c = txtbuff[l];
        int m_FCPos = (c - m_FontBase) * m_FCBytes;
        
        // Serial.print(cc); Serial.print(" "); 
        // Serial.print(c); Serial.print(" "); 
        // Serial.print((int)c); Serial.print(" "); 
        // Serial.print(m_FCPos); Serial.println(" ");
        
        
        // Convertir a Leds
        for (int b=0; b < m_FCBytes; b++){
            int idx = (m_FCPos + b);
            int pp = pgm_read_byte_near(m_FontData + idx);

            // Serial.print(idx); Serial.println(" ");
            // Serial.print(pp, BIN); Serial.println(" ");
            
            for (int bit = 0;  bit < m_FontWidth; bit++){
                int pos = ((b * BUFFER_LED) + l * 8 + bit);
                int bit_val = pp & (0b10000000>>bit);
                // Serial.print(bit);
                // Serial.print(": ");
                // Serial.print(bit_val, BIN);
                // Serial.print("-> ");
                if (bit_val != 0){
                    // Serial.print("B");
                    ledChar[pos] = true;
                }else{
                    // Serial.print("N");
                    ledChar[pos] = false;
                }
                // Serial.print(b); Serial.print("->");Serial.print(l); Serial.print("->");Serial.print(bit); Serial.print("->");
                // Serial.print(((b * BUFFER_LED) + l * 8 + bit)); Serial.print(": ");
                // Serial.print(ledChar[((b * BUFFER_LED) + l * 8 + bit)]); Serial.println(" ");
            }
            // Serial.println("-------");
        }
    }
    

    // delay(10000);   
    
    // Serial.println(F("Entramos en Matrix"));
    // Serial.print(F("x: ")); Serial.print(x); Serial.print(F(" y: ")); Serial.println(y); 
    // Serial.print(F("m_FH: ")); Serial.print(m_FontHeight); Serial.print(F(" L: ")); Serial.println(leds_w); 

    const byte fadeAmt = 220;
    
    // Draw it
    for (int t = 0; t < (m_FontHeight * BUFFER_LED); t++)
    {
        // Serial.print(x+t);
        // Serial.print(" ");

        int n_y = y + t % BUFFER_LED;
        // Serial.print(t); Serial.print("->"); Serial.print(n_y); Serial.print("->"); Serial.print(y); Serial.print("->"); Serial.print(ledChar[t]);Serial.println(""); 
    
        if (n_y >= 0 && n_y < GRID_W && ledChar[t] )
        {
            int position = (GRID_H * GRID_W - 1) - ((x + (int) (t/ BUFFER_LED)) * GRID_W + n_y);
            leds[position] = textColor;
            // Serial.print(t);
            // Serial.print("->");
            // Serial.print(n_y);
            // Serial.print("->");
            // Serial.print(y);
            // Serial.print("->");
            // Serial.print(position);
            // Serial.print("->");
            // Serial.print(leds[position], HEX);
            // Serial.print(" ");
            // if (t%leds_w == (leds_w-1) )
            //     Serial.println("");
        }
    }
    // delay(5000);
    if (y < leds_w * -1) {
        y = 15;
    }

    // Degrada la cola
    FastLED.show();

    for(int j = 0; j < NUM_LEDS; j++){
        leds[j] = leds[j].fadeToBlackBy(fadeAmt);
        // leds[j+1] = leds[j+1].fadeToBlackBy(fadeAmt+10);
        }
    
    delay(50);
    // FastLED.show();
    // FastLED.clear(false);
        // Degrada la cola
        
        // for(int j = 0; j < NUM_LEDS; j++)
        // if (random(2) == 1)
        //     leds[j] = leds[j].fadeToBlackBy(fadeAmt);

        // Serial.println("");

    // Serial.println(F("Salimos DrawText"));
    // Serial.print("Txt: ");
    // Serial.println(txt1);
    y--;
}
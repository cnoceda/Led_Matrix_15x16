/***********************************************
 * 
 *  Carlos Noceda - (c) 2020
 * 
 * File:    main.cpp
 * 
 * Description:
 *    Juego de efectos para la matriz de 15x16 en principio he escojido 4:
 *    1 - Marquee 
 *    2 - Commet
 *    3 - Bouncing Balls
 *    4 - Scrolling Text
 *    He conectado un modulo de Bluetooth HC-05 para el cambio y la configuracion de los efectos. 
 * 
 * History:   20201130  cnoceda   Subido a github
 *    Estoy teniendo problemas con la memoria. El bufffer de texto (50 caracteres) no le gusta. 
 *    Estoy trabajando en una nueva version del efecto, que ahorre memoria. 
 *    El efecto de las bolas tambien acupo mucho espacio. ** Eliminado **
 * 
 * TODOS:
 *    [X] - Revisar el efecto Text Scroll
 *    [x] - Revisar el efecto Bouncing Balls, quizas ni siquiera me guste asi. ELIMINADO. Mucha memoria para lo que es. 
 *    [X] - Solventado lo de la memoria revisar el bluetooth, da problemas, aunque creo que es por el uso de la memoria
 *          de los efectos anteriores.
 * 
 ***********************************************/

#include <Arduino.h>
#define FASTED_INTERNAL
#include <FastLED.h>
#include <SoftwareSerial.h>

// #define DEBUG
#include "DebugUtils.h"

#define DISPLAY_TEST  /* define to show test patterns at startup */
#define GRID_W 16
#define GRID_H 15
#define NUM_LEDS (GRID_W * GRID_H)
#define BRIGHTNESS 64
#define FPS 15


#define DATA_PIN 2
#define TxD 3
#define RxD 4


CRGB leds[NUM_LEDS] = {CRGB::Black};
SoftwareSerial bluetoothSerial(TxD, RxD);
static char receivedChars[25];
boolean clearFastLED = false;

int opt = 3;
long txtSpeed = 75;
bool txtrainbow = false;
const byte fadeAmt = 230;

// Incluimos los efectos aqui
#include "marquee.h"
#include "comet.h"
// #include "bounce.h"
// #include "text_scroll.h"
// #include "text_scroll_v2.h"
#include "text_scroll_v3.h"
//#include "tetris.h"
#include "sprite_draw.h"

// Cadenas y ayudas para la gestion de opciones de Bluetooth
CRGB textColor = CRGB::Red;
const char menu1[] PROGMEM = "Menu\n====\n\n1-Marquee\n2-Comet \n3-Sprite\n4-Scrool text\n";
const char menu2[] PROGMEM = "T-Cambia el texto\nS-Cambia la velocidad scroll (ms)\nC-Cambia el color del texto\n";
const char m_color[] PROGMEM= "CR-Rainbow\nC0-Verde\nC1-Azul\nC2-Rojo\nC3-Aleatorio\n";
char myChar;

/***************************************************************************************
 * Estas cadenas estan relacionadas entre si. 
 * startMarkers almacena las opciones del menu.
 * optionNumber relaciona cada opcion de menú con el efecto correspondiente
 * multivalue es 0 si la opcion de menú no admite mas valores y 1 si adminte mas valores
 ****************************************************************************************/
const char startMarkers[] = "H1234TSC";
const char optionNumber[] = "01234444";
const char multivalue[]   = "00000111";
const char endMarker = 0xD;

void recvWithStartEndMarkers() {
  /* Esta funcion recoje los valores del puerto de Bluetooth y los procesa
   * Si recive alguna opcion multivalor, la procesa tambien. 
   * La forma de leer los mensajes y mandar las respuestas al bluetooth tuve que tunearla mucho
   * el uso de Strings por ejemplo ocupa bastante memoria y no se gestiona muy bien si esa cadena la cambias 
   * de tamaño muchas veces. 
   * Asi que la mejor opcion es enviar uno a uno los caracteres. 
   */
    static boolean recvInProgress = false;
    static byte ndx = 0;
    const char *ptr;
    char rc, firstChar;
    int index, opt_old;
    // DEBUG_PRINTLN(F("Reading BT...."));
    
    while (bluetoothSerial.available() > 0 ) {
        // DEBUG_PRINTLN(F("Inside BT...."));
        rc = bluetoothSerial.read();
        // DEBUG_PRINTHEX(F("rc: "),rc);
        // DEBUG_PRINTDEC(F("recvInProgress: "),recvInProgress);
        ptr = strchr(startMarkers, rc);
        if (recvInProgress == true) {
            if (rc != endMarker) {
                receivedChars[ndx] = rc;
                ndx++;
                if (ndx >= MAX_TEXT) {
                    ndx = MAX_TEXT - 1;
                }
            }
            else {
                DEBUG_PRINTDEC(F("endMarker: "),rc);
                receivedChars[ndx] = '\0'; // terminate the string
                recvInProgress = false;
                ndx = 0;
                // bluetoothSerial.flush();
            }
        }

        else if (ptr) {
            index = ptr - startMarkers;
            DEBUG_PRINTCHR(F("Multivalue: "), multivalue[index]);
            DEBUG_PRINTCHR(F("rc: "), rc);
            DEBUG_PRINTDEC(F("index: "), index);
            firstChar = rc;
            
            if (multivalue[index] == '1')
            {
                recvInProgress = true;
            }
            else
            {
              recvInProgress = false;
          
              DEBUG_PRINTCHR(F("Opcion: "), rc);
              
              char optNum[2];
              
              memcpy(optNum, &optionNumber[index],1);
              optNum[2] = '\0';
              opt_old = opt;
              
              DEBUG_PRINTHEX(F("opt: "), opt);
              DEBUG_PRINTHEX(F("opt_old: "), opt_old);

              if ( isdigit(firstChar) ){
                opt =   atoi(optNum);
                DEBUG_PRINTHEX(F("atoi: "), opt);

                if (opt == opt_old || opt == 0){
                    opt = opt_old;
                }
                else{
                  char msg[17] = "N opt: ";
                  char buffer[4];
                  sprintf(buffer, "%d", opt);  
                  strcat(msg, buffer);
                  strcat(msg, "\n");
                  bluetoothSerial.write(msg);
                  clearFastLED = true;
                }
              }

              if (isalpha(firstChar) ){
                if (firstChar == 'H'){
                  for (byte k = 0; k < strlen_P(menu1); k++) {
                    myChar = pgm_read_byte_near(menu1 + k);
                    bluetoothSerial.write(myChar);
                  }

                  for (byte k = 0; k < strlen_P(menu2); k++) {
                    myChar = pgm_read_byte_near(menu2 + k);
                    bluetoothSerial.write(myChar);
                  }

                  bluetoothSerial.flush();
                }
              }
            }
        }
    }
    
    // DEBUG_PRINTCHR(F("receivedChars: "),receivedChars);
    if (strlen(receivedChars) != 0){
      DEBUG_PRINTCHR(F("firstChar: "), firstChar);
      switch (firstChar)
      {
        case 'T':
          {
            bluetoothSerial.write("Op T: \n");
            strupr(receivedChars);
            
            if (strlen(receivedChars) >1)
            {
                strcpy(txtbuff, receivedChars);
            }

            DEBUG_PRINTCHR(F("New_text: "), receivedChars);
            DEBUG_PRINTCHR(F("txtbuff: "), txtbuff);
            DEBUG_PRINTCHR(F("txtbuff len: "), strlen(txtbuff));
            break;
          }
      
        case 'S':
          {
            bluetoothSerial.write("Opcion S: \n");
            int speed =  atoi(receivedChars);
            
            if ( speed == 0)
            {
              bluetoothSerial.write("Dato incorrecto\n");
            }
            else
            {
              txtSpeed = speed;
            }

            DEBUG_PRINTDEC(F("Speed: "), speed);
            break;
          }
        
        case 'C':
          {
            bluetoothSerial.write("Op C:\n");
            DEBUG_PRINTLN(F("In C"));
            DEBUG_PRINTCHR(F("RChars "),receivedChars[0]);
            
            if (receivedChars[0] == 'R'){
              bluetoothSerial.write("Op. CR: \n");
              txtrainbow = true;
              DEBUG_PRINTDEC(F("Rbw: "), txtrainbow);
            }

            if (receivedChars[0] == 'r'){
              bluetoothSerial.write("Op. CR: \n");
              txtrainbow = false;
              DEBUG_PRINTDEC(F("Rbw: "), txtrainbow);
            }

            if (receivedChars[0] == '0'){
              bluetoothSerial.write("Op. CR:\n");
              textColor = CRGB::Green;
            }
            if (receivedChars[0] == '1'){
              bluetoothSerial.write("Op. CR:\n");
              textColor = CRGB::Blue;
            }
            if (receivedChars[0] == '2'){
              bluetoothSerial.write("Op. CR:\n");
              textColor = CRGB::Red;
            }
            if (receivedChars[0] == '3'){
              bluetoothSerial.write("Op. CR: \n");
              textColor = CHSV(random8(),255,255);
              char color[16];
              sprintf(color, "r:%i g:%i b:%i\n", textColor.r, textColor.g,textColor.b);
              bluetoothSerial.write(color);
            }

            if (receivedChars[0] == 'H'){
              for (byte k = 0; k < strlen_P(m_color); k++) {
                myChar = pgm_read_byte_near(m_color + k);
                bluetoothSerial.write(myChar);
              }
              bluetoothSerial.flush();
            }
            break;
          }
        
        default:
          break;
      }

    }
    memset(receivedChars,0,strlen(receivedChars));
    bluetoothSerial.flush();
}

void setup() {
  // put your setup code here, to run once:
    #ifdef DEBUG
      Serial.begin(9600);
      Serial.println("setup()");
    #endif
    bluetoothSerial.begin(9600);

    FastLED.addLeds<WS2812B, DATA_PIN, GRB>(leds, NUM_LEDS);
    FastLED.setBrightness(BRIGHTNESS);
    
    // Linea eliminada del efecto Bounce()
    // InitBalls();
    
    #ifdef DISPLAY_TEST
      FastLED.clear();
      for (uint16_t i=0; i<GRID_H ; ++i ) {
        leds[pos(i,0)] = CRGB::White;
        FastLED.show();
        delay(1000/FPS);
        leds[pos(i,0)] = 0;
      }
      for (uint16_t i=0; i<GRID_W; ++i ) {
        leds[pos(0,i)] = CRGB::White;
        FastLED.show();
        delay(1000/FPS);
        leds[pos(0,i)] = 0;
      }
    #endif

    FastLED.clear();
    FastLED.show(); 

}

void loop() {
    // Leemos el Bluetooth
    recvWithStartEndMarkers();

    // Si el efecto cambia, limpiamos la matriz
    if (clearFastLED){
        DEBUG_PRINTLN(F("Borrar leds"))
        FastLED.clear(false);
        clearFastLED = false;
    }
    
    // Seleccionamos efecto según la opción.
    switch (opt){
      case 1:
        DrawMarquee();
        break;
      case 2:
        DrawComet();
        break;
      case 3:
        DrawSprites();
        // delay(200);
        break;
      case 4:
        DrawText(txtbuff, txtrainbow);
        delay(txtSpeed);
        break;
      case 5:
        break;
    }
       
    FastLED.show();
    
    DEBUG_PAUSE(200);
}


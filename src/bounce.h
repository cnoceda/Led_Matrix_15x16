//+-------------------------------------------------------------------------
//
// Carlos Noceda - (c) 2020
//
// File:    bounce.h
//
// Description:
//
//  Pelotas rebotando
//
//
// History:     20201120  cnoceda   creado
//              20201130  cnoceda   El poner 16 pelotas una por columna agota la memoria. 
//--------------------------------------------------------------------------

#include <time.h>  
#include <Arduino.h>
#define FASTED_INTERNAL
#include <FastLED.h>

// #define DEBUG
#include "DebugUtils.h"

extern CRGB leds[];

#define ARRAYSIZE(x) (sizeof(x)/sizeof(x[0]))  // Cuenta los elementos de un static array
#define NUM_BALLS 2

const CRGB ballColors [] =
{
    CRGB::Green,
    CRGB::Red,
    CRGB::Blue,
    CRGB::Yellow,
    CRGB::Purple,
    CRGB::OrangeRed,
    CRGB::Cyan,
};

const size_t  _cLength = 15;
const size_t  _cBalls = NUM_BALLS;
const byte    _fadeRate = 64;
bool   _bMirrored = false;
bool inicializado = false;
static double ClockTimeAtLastBounce[NUM_BALLS] = {0.0};
static double Height[NUM_BALLS]  = {0.0};
static double BallSpeed[NUM_BALLS] = {0.0};
static double Dampening[NUM_BALLS] = {0.0};
static CRGB Colors[NUM_BALLS] = { CRGB::Black };
const double Gravity = -9.81;      // La gravedad :-)
const double StartHeight = 1;     // Las pelotas caen desde la maxima altura
const double SpeedKnob = 2.0;     // Valores altos menos velocidad

double InitialBallSpeed(double height)
{
    return sqrt(-2 * Gravity * height);    // Matematicas!
}


static double Time() //const
{
    return (double) millis() / 1000; //(double)(tv.tv_usec / 1000000.0 + (double) tv.tv_sec);
}

// Draw
//
// Dibuja cada una de las bolas. Cuando una bola tiene poca energia, la reiniciamos
void DrawBalls()
{
    
    if (_fadeRate != 0)
    {

        for (size_t p = 0 ; p < NUM_LEDS; p++){            
            // DEBUG_PRINTDEC(F("bola ant: : "), antBall)
            // DEBUG_PRINTDEC(F("pos inicial: "), p)
            leds[p].fadeToBlackBy(_fadeRate);
        }

    }

    // Dibujar las bolas
    for (size_t i = 0; i < _cBalls; i++)
    {   

        double TimeSinceLastBounce = (Time() - ClockTimeAtLastBounce[i]) / SpeedKnob;
        DEBUG_PRINTDEC(F("TimeSinceLastBounce: "), TimeSinceLastBounce);
        DEBUG_PRINTDEC(F("BallSpeed: "), BallSpeed[i]);
        // Usar la funcion de aceleracion estandar
        Height[i] = 0.5 * Gravity * pow(TimeSinceLastBounce, 2.0) + BallSpeed[i] * TimeSinceLastBounce;
        DEBUG_PRINTDEC(F("Height: "), Height[i]);
        
        // la pelota golpea el suelo
        if (Height[i] < 0)
        {   
            Height[i] = 0;

            BallSpeed[i] = Dampening[i] * BallSpeed[i];

            ClockTimeAtLastBounce[i] = Time();
            DEBUG_PRINTDEC(F("TIME: "),ClockTimeAtLastBounce[i])
            if (BallSpeed[i] < 0.5)
                BallSpeed[i] = InitialBallSpeed(StartHeight) * Dampening[i];
        }

        size_t position = (size_t)(Height[i] * (_cLength - 1) / StartHeight) + i * _cLength;

        leds[position] += Colors[i];
        leds[position + 1] += Colors[i];
        // DEBUG_PRINTDEC(F("Mirror "), _cLength);
        DEBUG_PRINTDEC(F("ball: "), i);
        DEBUG_PRINTDEC(F("BallSpeed[i]: "), BallSpeed[i]);
        // DEBUG_PRINTDEC(F("StartHeight: "), StartHeight);
        DEBUG_PRINTDEC(F("position: "), position);
        // DEBUG_PRINTDEC(F("positionR: "), (_cLength + (i * 15)) - 1 - (position-(i * 15)));
        if (_bMirrored)
        {
            leds[(_cLength + (i * 15)) - 1 - (position-(i * 15))]   += Colors[i];
            leds[(_cLength + (i * 15))- (position-(i * 15))]        += Colors[i];
        }
        
    }
           
    DEBUG_PAUSE(500);
}
void InitBalls()
{
    DEBUG_PRINTLN(F("Entrando en InitBalls..."))
    DEBUG_PRINTDEC(F("inicializado: "), inicializado);

    if (!inicializado){

        // BouncingBallEffect
        //
        // Caller specs strip length, number of balls, persistence level (255 is least), and whether the
        // balls should be drawn mirrored from each side.

        for (size_t i = 0; i < _cBalls; i++)
            {
            Height[i]                 = StartHeight;
            ClockTimeAtLastBounce[i]  = Time();
            Dampening[i]              = 0.9 - i / pow((_cBalls),2);
            BallSpeed[i]              = InitialBallSpeed(Height[i]);
            Colors[i]                 = ballColors[i % 7];

            DEBUG_PRINTDEC(F("Bola: "), i);
            DEBUG_PRINTDBL(F("Height[i]: "), Height[i]);
            DEBUG_PRINTDBL(F("ClockTimeAtLastBounce[i]: "), ClockTimeAtLastBounce[i]);
            DEBUG_PRINTDBL(F("Dampening[i]: "), Dampening[i]);
            DEBUG_PRINTDBL(F("BallSpeed[i]: "), BallSpeed[i]);
            DEBUG_PRINTDBL(F("Colors[i]: "), Colors[i]);
            DEBUG_PRINTDBL(F("ARRAYSIZE(ballColors): "), ARRAYSIZE(ballColors));

            }
        
        inicializado = true;
    
    } 
}

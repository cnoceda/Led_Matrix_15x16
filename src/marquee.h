//+-------------------------------------------------------------------------
//
// Carlos Noceda - (c) 2020
//
// File:    marquee.h
//
// Description:
//
//  Dibuja una marquee en colores.
//
//
// History:   20201120  cnoceda   creado
//
//--------------------------------------------------------------------------
#include <Arduino.h>
#define FASTED_INTERNAL
#include <FastLED.h>

void DrawMarquee()
{
    static byte j = HUE_BLUE;
    j += 4;
    byte k = j;

    // Esto es el equivalente a fill_rainbow(leds, NUM_LEDS, j, 8)

    CRGB c;
    for(int i = 0; i < NUM_LEDS; i++)
    {
      leds[i] = c.setHue(k+=8);
    }

    static int scroll = 0;
    scroll++;
    
    for(int i = scroll % 7; i < NUM_LEDS - 1; i += 7)
    {
      leds[i] = CRGB::Black;
      leds[i+1] = CRGB::Black;
    }
      
    delay(100);
  
}

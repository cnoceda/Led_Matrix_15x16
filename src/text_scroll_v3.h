//+-------------------------------------------------------------------------
//
// Carlos Noceda - (c) 2020
//
// File:    text_scroll_v3.h
//
// Description:
//
//  Texto desplazandose en la matriz.
//  Cambio de array de almacenamiento para ahorro de memoria
//
// History:   20201127  cnoceda   creado 
//      Modificacion de la version anterior para ahorrar memoria, ahora pasamos el buffer 
//      temporal de led a nivel bit ahorrando un 82% de memoria. 
//      sigo con problemas de memoria, voy a renderizar solo el texto de la matriz. En la v3
//      Ahora solo renderizamos la parte que se vé en la matriz, con lo que se ahorra mas memoria
//      aunque se complica el codigo un poco. Quizas este algo desordenado. Pero he metido muchas 
//      explicaciones.
// TODOS:
//      [X] Jugar con los colores. Tampoco me he matado :-)
//--------------------------------------------------------------------------

#include <Arduino.h>
#define FASTED_INTERNAL
#include <FastLED.h>
#include <FontRobotron.h>

#define DEBUG
#include "DebugUtils.h"

#define MAX_TEXT 50
#define BUFFER_LED (((7 * MAX_TEXT)))
#define POS_X  4

//texto por defecto
static char txtbuff[MAX_TEXT] = "** WELCOME **";

//Esto es para recordar las posiciones :-)
// Init font. Formato:
// const uint8_t RobotronFontData[] = {
// 				7,		// Font Width
// 				7,		// Font Height
// 				32,		// Font First Character
// 				96,		// Font Last Character
const uint8_t *FontData = RobotronFontData;

static int y = 10;

extern CRGB textColor;

void DrawFont(char c, int bit, int actual_char){
    
    DEBUG_PRINTLN("");
    DEBUG_PRINTDEC(F("bit: "), bit);
    DEBUG_PRINTDEC(F("actual_char: "), actual_char);
    DEBUG_PRINTDEC(F("y: "), y);

    uint8_t  m_FWBytes = 0, m_FCBytes;
    // Puntero al Font
    const uint8_t *m_FontData;

    DEBUG_PRINTDEC(F("Width: "), FontData[0]);
    DEBUG_PRINTDEC(F("Heigth: "), FontData[1]);
    DEBUG_PRINTDEC(F("First: "), FontData[2]);
    DEBUG_PRINTDEC(F("Last: "), FontData[3]);
    
    m_FontData = &FontData[4];
    m_FWBytes = (FontData[0] + 7) / 8;
    m_FCBytes += (m_FWBytes * FontData[1]);
    DEBUG_PRINTDEC(F("m_FCBytes: "), m_FCBytes);
    DEBUG_PRINTDEC(F("m_FWBytes: "), m_FWBytes);
    
    // Recorremos los caracteres del texto
    if ((int) c >= FontData[2] && (int) c <= FontData[3])
    {   
        // Encontrar la posicion del Font en el fichero
        int m_FCPos = (c - FontData[2]) * m_FCBytes;
        
        DEBUG_PRINTLN(F("NEW CHAR ============"));
        DEBUG_PRINT(c); DEBUG_PRINT((int)c); 
        DEBUG_PRINT(m_FCPos); DEBUG_PRINTLN("");
        DEBUG_PRINTLN(F("====================="));
        
        // Cargar el caracter al buffer
        for (int b=0; b < m_FCBytes; b++){
            int idx = (m_FCPos + b);
            uint8_t pp = pgm_read_byte_near(m_FontData + idx);

            for (int bit_lin = 0; bit_lin < bit + 1 ; bit_lin++)
            {
                // El valor del bit lo hacemos con el desplazamiento de 
                // DEBUG_PRINTCHR(F("Y: "),y);DEBUG_PRINTCHR(F("bit_lin: "),bit_lin);DEBUG_PRINTCHR(F("b: "),b);
                
                int n_y = (y + bit_lin + (actual_char -1)* 8) ; 
                DEBUG_PRINTNOTLN("n_y: ");DEBUG_PRINTLN(n_y);
                DEBUG_PRINTDEC(F("bit_lin: "), bit_lin);
                int bit_val = pp & (0b10000000 >> bit_lin);
                if (n_y >= -1 && n_y < GRID_H && bit_val){
                    int position = (GRID_H * GRID_W - 1) - ((GRID_H-1-n_y) * GRID_H + POS_X + b);
                    DEBUG_PRINT(F(" position: ")); DEBUG_PRINT(position); DEBUG_PRINTLN(" 1");
                    // #ifndef DEBUG
                        leds[position] = textColor;
                    // #endif
                }
            }

            DEBUG_PRINTLN("-------------");
        }
    }
    DEBUG_PRINTLN("Fin DrawFont");
}


static void DrawText(char* txt1, bool rainbow=false){

    DEBUG_PRINTLN("Entramos en DrawText");
    DEBUG_PRINTCHR("Txt: ", txt1);
    DEBUG_PRINTDEC("Buffer_l: ", (strlen(txt1)));
    
    
    static byte j = HUE_BLUE;
    j += 4;
    byte k = j;
    
    if (rainbow){
        textColor.setHue(k+=8);
    }
    
    // Creamos la estela
    // Bajamos la intensidad de los leds pintado actualmente para que se 
    // vean mas apagados antes de pintar los nuevos

    
    for(int j = 0; j < NUM_LEDS; j++){
        leds[j] = leds[j].fadeToBlackBy(fadeAmt);
    }
    
    
    // el ancho del buffer, numero de caracteres
    int leds_w;
    
    // el ancho del buffer en bits
    int leds_bits_w;
    
    //numero de bits que se ven en cada momento
    int bits_vistos;

    // bit en el comienza en el buffer de texto
    int start_buffer_bit;

    // ultimo bit en el buffer de texto
    // int end_buffer_bit;

    // ancho en bits de la ventana del buffer
    // int w_buffer_window;

    // posicion del byte donde esta comienza a verse
    int start_byte;

    // offset de bits desde y 
    int bits_totales;

    // bit que hay que mostrar
    int bit_actual;

    // El ancho es el numero de caracteres, tal cual.
    leds_w = strlen(txt1);
    leds_bits_w = leds_w * 8;
    DEBUG_PRINTDEC(F("leds_bits_w: "), leds_bits_w); 

    // DEBUG_PRINT(sizeof(ledChar)); 
    DEBUG_PRINTDEC(F("leds_w: "), leds_w); 
    
    // por cada una de las lineas pintamos el caracter. 

    // Calculo del offset de bits desde Y
    bits_totales     = GRID_W - y;
    DEBUG_PRINTDEC(F("bits_totales: "), bits_totales); 
    // Caracter del buffer
    start_byte       = (int)floor(bits_totales / 8);
    DEBUG_PRINTDEC(F("start_byte: "), start_byte); 
    // bit a dibujar
    bit_actual       = bits_totales%8;
    DEBUG_PRINTDEC(F("bit_actual: "), bit_actual); 

    start_buffer_bit = (int)(bits_totales/GRID_W) - y -1;
    DEBUG_PRINTDEC(F("start_buffer_bit: "), start_buffer_bit); 
    
    // Numero de bits a mostrar        
    if (bits_totales > leds_bits_w)
    {
        bits_vistos  = leds_bits_w - start_buffer_bit;
    }
    else
    {
        bits_vistos = bits_totales;
    }
    DEBUG_PRINTDEC(F("bits_vistos: "), bits_vistos); 
    
    int num;
    
    for (num = 0; num < start_byte; num++)
    {   
        DEBUG_PRINTDEC(F("num: "), num);
        DEBUG_PRINTLN(F("Caracter ENTERO"));
        DrawFont(txt1[num], 7, num + 1 );
    }

    if (bit_actual != 0) 
    {
        DEBUG_PRINTDEC(F("num: "), num);
        DEBUG_PRINTLN(F("Caracter PARCIAL"));
        DrawFont(txt1[num], bit_actual, num + 1 );
    }

    // Delay para el Debug ;-)
    DEBUG_PAUSE(1000);
    
    // Si y supera el tamaño del ancho del mensaje, es que ha terminado de mostrarlo. 
    if (y < leds_w * 8 * - 1) 
        y = 15;
    else
        y--;
}

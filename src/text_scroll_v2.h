//+-------------------------------------------------------------------------
//
// Carlos Noceda - (c) 2020
//
// File:    text_scroll_v3.h
//
// Description:
//
//  Texto desplazandose en la matriz.
//  Cambio de array de almacenamiento para ahorro de memoria
//  Reduzco mas el array intermedio voy a intentar poner los leds directamente
//
// History:   20201127  cnoceda   creado 
//      Modificacion de la version anterior para ahorrar memoria, ahora pasamos el buffer 
//      temporal de led a nivel bit ahorrando un 82% de memoria. 
//      sigo con problemas de memoria, voy a renderizar solo el texto de la matriz. En la v3.

// TODOS:
//      Jugar con los colores
//--------------------------------------------------------------------------

#include <time.h>  
#include <Arduino.h>
#define FASTED_INTERNAL
#include <FastLED.h>
#include <FontRobotron.h>

#define DEBUG
#include "DebugUtils.h"


extern CRGB textColor;

// Tamaño del buffer
// El tamañño maximo por 7 filas cada caracter
static uint8_t ledChar[BUFFER_LED] = {0}; 

static void DrawText(char* txt1, bool rainbow=false){

    DEBUG_PRINTLN("Entramos en DrawText");
    DEBUG_PRINTCHR("Txt: ", txt1);
    DEBUG_PRINTDEC("Buffer_l: ", (strlen(txt1)));
    
    static int x = 4;
    static int y = 10;
    
    static byte j = HUE_BLUE;
    j += 4;
    byte k = j;
    
    if (rainbow){
        textColor.setHue(k+=8);
    }
    
    // Creamos la estela
    // Bajamos la intensidad de los leds pintado actualmente para que se 
    // vean mas apagados antes de pintar los nuevos

    
    for(int j = 0; j < NUM_LEDS; j++){
        leds[j] = leds[j].fadeToBlackBy(fadeAmt);
    }
    

    uint8_t   m_FWBytes = 0, m_FCBytes;
    const uint8_t *m_FontData;
    
    // el ancho del buffer
    int leds_w;
    // El ancho es el numero de caracteres, tal cual.
    leds_w = strlen(txt1);
    
    DEBUG_PRINT(sizeof(ledChar)); DEBUG_PRINT(leds_w); DEBUG_PRINTLN("");
     
    // Init font. Formato:
    // const uint8_t RobotronFontData[] = {
	// 				7,		// Font Width
	// 				7,		// Font Height
	// 				32,		// Font First Character
	// 				96,		// Font Last Character
    const uint8_t *FontData = RobotronFontData;
    // m_FontWidth = FontData[0];
    DEBUG_PRINTDEC(F("Width: "), FontData[0]);
    DEBUG_PRINTDEC(F("Heigth: "), FontData[1]);
    DEBUG_PRINTDEC(F("First: "), FontData[2]);
    DEBUG_PRINTDEC(F("Last: "), FontData[3]);
    // m_FontHeight = FontData[1];
    // m_FontBase = FontData[2];
    // m_FontUpper = FontData[3];
    m_FontData = &FontData[4];
    m_FWBytes = (FontData[0] + 7) / 8;
    m_FCBytes += (m_FWBytes * FontData[1]);
    DEBUG_PRINTDEC(F("m_FCBytes: "), m_FCBytes);
    DEBUG_PRINTDEC(F("m_FWBytes: "), m_FWBytes);
    
    // Recorremos los caracteres del texto
    for (int l = 0; l < strlen(txt1); l++)
    {   
        // Posicion en el buffer
        // leemos caracter y comprobamos que esta en fichero de Fonts
        char c = txt1[l];
        if ((int) c >= FontData[2] && (int) c <= FontData[3])
        {   
            // Encontrar la posicion del Font en el fichero
            int m_FCPos = (c - FontData[2]) * m_FCBytes;
            
            DEBUG_PRINTLN(F("NEW CHAR ============"));
            DEBUG_PRINT(c); DEBUG_PRINT((int)c); 
            DEBUG_PRINT(m_FCPos); DEBUG_PRINTLN("");
            
            // Cargar el caracter al buffer
            for (int b=0; b < m_FCBytes; b++){
                int idx = (m_FCPos + b);
                uint8_t pp = pgm_read_byte_near(m_FontData + idx);

                ledChar[l * m_FCBytes + b] = pp;
            
                DEBUG_PRINTLN(idx)
                DEBUG_PRINTBIN("",pp);
                DEBUG_PRINTDEC(F(""), (l * m_FCBytes + b)); 
                // DEBUG_PRINTLN(F("---- Byte"));
            }
            DEBUG_PRINTLN("-------");
        }
    }

    // Dibujar el la matriz led
    // calculamos cuanto hay que dibujar:
    // Todo el eje x y solo GRID_W - Y 
    // Para calcular los bytes/bit a recorrer del array 
    // (int) (GRID-y)/7  nos da el indice fin
    
    for (int b = 0, actual_char = 1; b <= (int) ((GRID_W - y) / 8) * 7; b += 7 , actual_char++){
        // los bit a leer de cada byte de linea es (GRID_W - y) % 8
        // sabemos que cada caracter son 7 bytes uno por linea
        // asi que hay que leer esos bits de cada una de las lineas. 

        // Primer byte del caracter dentro del array
        int pos_first_byte = b;
        DEBUG_PRINTDEC(F("first byte: "), pos_first_byte);
        for (int byte_lin = 0; byte_lin < FontData[1]; byte_lin++){
            // bucle para leer los bits de byte_lin
            // DEBUG_PRINTCHR(F("CHAR:"), txt1[actual_char-1]);        
            // DEBUG_PRINTDEC(F("char_Lin: "), byte_lin);
            // DEBUG_PRINTDEC(F("X:"), x);
            
            // Comprobar si el Byte tiene que ser completo
            // (int) (GRID_W - y) / 8 >= actual_char
            int num_bits = (int) (GRID_W - y) / 8;
            if ( (num_bits >= actual_char )){
                num_bits = 8;
                }
            else{ 
                num_bits = (GRID_W - y) % 8;
                }

            for (int bit_lin = 0; bit_lin < num_bits ; bit_lin++)
            {
                // El valor del bit lo hacemos con el desplazamiento de 
                DEBUG_PRINTCHR(F("Y: "),y);DEBUG_PRINTCHR(F("bit_lin: "),bit_lin);DEBUG_PRINTCHR(F("b: "),b);
                
                int n_y = (y + bit_lin + b + actual_char - 1); 
                DEBUG_PRINTNOTLN("Y: ");DEBUG_PRINTLN(n_y);
                // DEBUG_PRINTDEC(F("bit_lin: "), bit_lin);
                int bit_val = ledChar[pos_first_byte + byte_lin] & (0b10000000 >> bit_lin);
                if (n_y >= -1 && n_y < GRID_H && bit_val){
                    // int position = (GRID_H * GRID_W - 1) - (((x+byte_lin) * GRID_W + (n_y)));
                    int position = (GRID_H * GRID_W - 1) - ((GRID_H-1-n_y) * GRID_H + x + byte_lin);
                    DEBUG_PRINT(F(" position: ")); DEBUG_PRINT(position); DEBUG_PRINTLN(" 1");
                    // #ifndef DEBUG
                        leds[position] = textColor;
                    // #endif
                }
            }
            DEBUG_PRINTLN("-------------")
        }
    }
    
    
    // Delay para el Debug ;-)
    DEBUG_PAUSE(5000);
    
    // Si y supera el tamaño del ancho del mensaje, es que ha terminado de mostrarlo. 
    if (y < leds_w * 8 * - 1) 
        y = 15;
    else
        y--;
    
}

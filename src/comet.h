//+-------------------------------------------------------------------------
//
// Carlos Noceda - (c) 2020
//
// File:    comet.h
//
// Description:
//
//  Dibuja una cometa con la cola difuminada. 
//
//
// History:   20201120  cnoceda   creado
//
//--------------------------------------------------------------------------
#include <Arduino.h>
#define FASTED_INTERNAL
#include <FastLED.h>

void DrawComet()
{
  //  FastLED.clear(false);

    const byte fadeAmt = 64;   // Una fraccion de 256 para oscurecer un pixel si le toca
    const int cometSize = 3;    // Tamañño de la cometa
    const int deltaHue = 4;     // 
    const double cometSpeed = 0.5; // Cuanto avanza la cometa en cada frame

    static byte hue = HUE_RED;    // Current color
    static int iDirection = 1;    // Current Direction (-1 or +1)
    static double iPos = 0.0 ;    //Current comet position en la cinta

    hue += deltaHue;             // actualiza el Hue
    iPos += iDirection * cometSpeed;         //actualiza la posición

    // Cambia la direccion 
    if (iPos == (NUM_LEDS - cometSize) || iPos ==0)
      iDirection *= -1;

    // Dibuja la cometa
    for(int i = 0; i < cometSize; i++)
      leds[(int)iPos + i].setHue(hue);

    // Degrada la cola
    for(int j = 0; j < NUM_LEDS; j++)
      if (random(2) == 1)
        leds[j] = leds[j].fadeToBlackBy(fadeAmt);
    
    
    // delay(10);
  
}

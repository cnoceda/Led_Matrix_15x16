//+-------------------------------------------------------------------------
//
// Carlos Noceda - (c) 2020
//
// File:    sprite_draw.h
//
// Description:
//
//  Dibuja una secuencia de sprites definidos 
//
// History:   20201201  cnoceda   creado 
// 
//--------------------------------------------------------------------------
#include <Arduino.h>
#define FASTED_INTERNAL
#include <FastLED.h>
#include <Arbol.h>

// #define DEBUG
#include "DebugUtils.h"

extern CRGB leds[];

// Sprite config
// 					15, 	// Sprite Width
// 					16,		// Sprite Heghh
// 					11,		// Numero de Fotogramas
// 					18,		// Numero Fotogramas de la secuencia
// 					1,2,1,3,1,2,1,3,1,2,4,5,7,8,9,10,11,0, // Secuencia
const uint32_t *SpriteData = Arbol;
const int *SecuenciaData = s_Arbol;
static int actual_sec = 0;
// static int8_t os_row = 2;
// static int8_t os_col = -22;

/** Pos - convierte col/ row en la posicion del pixel. 
 *  (0,0) en la pos 0. En mi caso abajo a la izquierda.
 */
uint16_t pos( int row, int col ) {
    return ( row + col * GRID_H);
}

void DrawSprites(){

    FastLED.clear(false);

    DEBUG_PRINTDEC(F("Width: "), SpriteData[0]);
    DEBUG_PRINTDEC(F("Heigth: "), SpriteData[1]);
    DEBUG_PRINTDEC(F("Ftgrms: "), SpriteData[2]);
    DEBUG_PRINTDEC(F("NFotoSec: "), SecuenciaData[1]);
   
    int index = 3; // A los datos de los sprites
    DEBUG_PRINTDEC(F("index: "), index); 
    
    int tiempo = 2 + SecuenciaData[1]; // A los tiempos de la secuencia
    int idx_offset = 2 + SecuenciaData[1] * 2;
    
    DEBUG_PRINTDEC(F("tiempo: "), tiempo); 
    const uint32_t *m_SpriteData = &SpriteData[index];
    
    // int ini_secuencia = 4;
    // int fin_secuencia = 4 + SpriteData[3];
    
    int sprite_size = SpriteData[0] * SpriteData[1];
    DEBUG_PRINTDEC(F("Size: "), sprite_size);
    

    int s_height = (int) SpriteData[1];
    int s_width  = (int) SpriteData[0];

    int val;
    int val_tiempo;
    int s_col, s_row;
    int idx_row, idx_col;

    for(int n_sprite = 0; n_sprite < s_Arbol[0]; n_sprite++)
    {
        idx_row = idx_offset + SecuenciaData[1] * n_sprite * 2 ;
        idx_col = idx_row + SecuenciaData[1];
        // idx_col = 68;
        // idx_row = 46;
        DEBUG_PRINTDEC(F("idx_r: "), idx_row);
        DEBUG_PRINTDEC(F("idx_c: "), idx_col);

        val = pgm_read_word_near(SecuenciaData + 2 + actual_sec);
        val_tiempo = pgm_read_word_near(SecuenciaData + tiempo + actual_sec);
        s_col = pgm_read_word_near(SecuenciaData + idx_col + actual_sec);
        s_row = pgm_read_word_near(SecuenciaData + idx_row + actual_sec);

        DEBUG_PRINTDEC(F("Sprt_s: "), val);
        DEBUG_PRINTDEC(F("Sprt_t: "), val_tiempo);
        DEBUG_PRINTDEC(F("ActSec: "), actual_sec);
        DEBUG_PRINTDEC(F("s_col: "), s_col);
        DEBUG_PRINTDEC(F("s_row: "), s_row);

        if (val != 0){
            for (int row = 14 ; row > (14 - s_height); row--){
                DEBUG_PRINTDEC(F("row: "), row);
                
                for (int col = 0; col < s_width ; col++)
                {
                    DEBUG_PRINT(F("col:"));DEBUG_PRINT(col);
                    uint32_t p_val;
                    int p_index = (val-1) * sprite_size + col +((SpriteData[0]) * (14-row));
                
                    // DEBUG_PRINTDEC(F("p_in: "), p_index);
                    // actua_sec%s_Fantasma[0] Daria la posicion de donde colocar el sprite. 
                    // int actual_r = row - os_row;
                    // int actual_c = col + os_col;
                    int actual_r = s_row - (14-row);
                    int actual_c = col + s_col;

                    // actual_c = actual_c + (actual_sec%s_Fantasma[0]) * s_width ;
                    
                    DEBUG_PRINT(F("a_c:"));DEBUG_PRINT(actual_c);
                    DEBUG_PRINT(F("a_r:"));DEBUG_PRINT(actual_r);
                    DEBUG_PRINTDEC(F("pos:"), pos(actual_r, actual_c));

                    if ((actual_r >= 0 && actual_r < GRID_H) && (actual_c >= 0 && actual_c < GRID_W)) {
                        p_val = pgm_read_dword_near(m_SpriteData + p_index);
                        DEBUG_PRINTHEX(F("p_val: "), p_val);
                        if (!(leds[pos(actual_r, actual_c)])){
                            leds[pos(actual_r, actual_c)] = CRGB(p_val);
                            }
                    }
                }
                DEBUG_PRINTLN("");
            }
            delay(val_tiempo);
        } 
        else{
            // FastLED.clear();
        }
    

        if (actual_sec < (SecuenciaData[1]-1))
        {
            actual_sec++;
        }
        else{
            actual_sec = 0;
        }

        
        
        // if (os_col < GRID_H)
        // {
        //     os_col++;
        //     DEBUG_PRINTDEC(F("os_col: "), os_col);
        // }
        // else
        // {
        //     os_col = (s_width * s_Fantasma[0]) * -1;
        //     DEBUG_PRINTDEC(F("rst_col: "), os_col);
        // }

    }
}


